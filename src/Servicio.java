
public class Servicio extends Producto{

	float porcentajeDescuento;
		
	public Servicio(String nombre,float precio,float porcentajeDescuento) {
		this.nombre=nombre;
		this.precio=precio;
		this.porcentajeDescuento=porcentajeDescuento;
	}
	
	public float calcularTarifa() {
		return precio - (precio*(porcentajeDescuento/100));
	}

}
