import java.util.*; 
public class Carrito {
	
	List<Producto> listaDeProductos = new ArrayList<Producto>(); 


	public List<Producto> getListaDeProductos(){
		return listaDeProductos;
	}
	
	public void añadirProducto(Producto producto) {
		
		listaDeProductos.add(producto);
	}
	
	public float calcularTotalAPagar() {
		
		float totalAPagar=0;
		Iterator<Producto> itr = listaDeProductos.iterator(); 
		while (itr.hasNext()) 
        { 
            Producto producto = (Producto)itr.next(); 
            totalAPagar= totalAPagar + producto.calcularTarifa();
        } 
		return totalAPagar;
	}
}
