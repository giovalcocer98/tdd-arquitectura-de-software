import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

class test {

	Carrito carrito = new Carrito();
	
	@Test
	public void retornaToallasSiSeCompraronToallas() {
		Producto articuloToallas= new Articulo("toallas",50);
		assertEquals("toallas",articuloToallas.nombre);
	}
	
	@Test
	public void retorna50SiSeCompraronToallasAlPrecioDe50() {
		Producto articuloToallas= new Articulo("toallas",50);
		assertEquals(50,articuloToallas.calcularTarifa());
	}
	
	@Test
	public void retornaLimpiezaSiSeComproElServicioDeLimpieza() {
		Producto ServicioLimpieza= new Servicio("Servicio de limpieza",200,20);
		assertEquals("Servicio de limpieza",ServicioLimpieza.nombre);
	}
	
	@Test
	public void retorna180SiSeComproElServicioDeLimpiezaA200Con10PorCientoDeDescuento1() {
		Producto ServicioLimpieza= new Servicio("Limpieza",200,10);
		assertEquals(180,ServicioLimpieza.calcularTarifa());
	}
	
	@Test
	public void retornaTrueSiSeAñadioUnArticuloAlCarritoDeCompras() {
		Producto articuloPan= new Articulo("Pan",10);
		carrito.añadirProducto(articuloPan);
		assertEquals(true,carrito.getListaDeProductos().contains(articuloPan));
	}
	
	@Test
	public void retorna10SiSeAñadioUnArticuloAlCarritoDeComprasConCosteDe10() {
		Producto articuloPan= new Articulo("Pan",10);
		carrito.añadirProducto(articuloPan);
		assertEquals(10,carrito.getListaDeProductos().get(0).precio);
	}
	
	@Test
	public void retornaPanSiSeAñadioElArticuloPanAlCarritoDeCompras() {
		Producto articuloPan= new Articulo("Pan",10);
		carrito.añadirProducto(articuloPan);
		assertEquals("Pan",carrito.getListaDeProductos().get(0).nombre);
	}
	
	
	@Test
	public void retornaTrueSiSeAñadioUnServicioAlCarritoDeCompras() {
		Producto servicioElectrico= new Servicio("Servicio electrico",150,20);
		carrito.añadirProducto(servicioElectrico);
		assertEquals(true,carrito.getListaDeProductos().contains(servicioElectrico));
	}
	
	@Test
	public void retorna300SiSeAñadioUnServicioAlCarritoDeComprasConCosteDe300() {
		Producto servicioElectrico= new Servicio("Servicio electrico",300,10);
		carrito.añadirProducto(servicioElectrico);
		assertEquals(300,carrito.getListaDeProductos().get(0).precio);
	}
	
	@Test
	public void retornaElectricoSiSeAñadioElServicioElectricoAlCarritoDeCompras() {
		Producto servicioElectrico= new Servicio("Servicio electrico",300,10);
		carrito.añadirProducto(servicioElectrico);
		assertEquals("Servicio electrico",carrito.getListaDeProductos().get(0).nombre);
	}
	
	@Test
	public void retorna10SiSeAñadioElServicioElectricoCon20PorCientoDeDescuentoAlCarritoDeCompras() {
		Producto servicioElectrico= new Servicio("Servicio electrico",500,20);
		carrito.añadirProducto(servicioElectrico);
		assertEquals(20,((Servicio) carrito.getListaDeProductos().get(0)).porcentajeDescuento);
	}
	
	
	@Test
	public void retorna1200SiElCarritoTieneProductosCuyaSumaDeTarifaEs1200() {
		Producto servicioElectrico= new Servicio("Servicio electrico",500,20);
		Producto servicioTecnico= new Servicio("Servicio tecnico",800,15);
		Producto articuloComedor= new Articulo("Comedor",100);
		Producto articuloCroquetas= new Articulo("Croquetas",20);
		
		carrito.añadirProducto(servicioElectrico);
		carrito.añadirProducto(servicioTecnico);
		carrito.añadirProducto(articuloComedor);
		carrito.añadirProducto(articuloCroquetas);
		assertEquals(1200,carrito.calcularTotalAPagar());
	}
	
	
	
	
	
	

}
